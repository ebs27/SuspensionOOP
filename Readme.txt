Suspension System LabVIEW Application
Created by: Eric Stach (ebs27@duke.edu)
Duke University, Mechanical Engineering and Materials Science
___________________________________________________________________________
Background: 
The suspension system is an example of the classic double spring-mass-damper system. There are three levels: the bottom (or road) level, the middle (or suspension) level, and top (or passenger) level. See the image file on the repository for a picture of the system.

The bottom level position is controlled by an air cylinder and a PID servo value regulates the air flow. The PID servo values receives an input voltage (0-10 V) signal from an external Enfield PID Module. The user can specify this voltage and as a result control the position of the bottom level. A sensor measures the position of this level, which provides feedback to the PID module. This sensor output is also looped back into the NI DAQ board to monitor position in the application.

The middle and top levels are connected by compression springs and vibrate as a result of the motion of the bottom level. Sensors measure the position and acceleration of 
both of these levels. 
_____________________________________________________________________________
Application Description:
This application controls the suspension system, collects data, and displays collected data sets. The application interacts with the system via the following DAQ connections:

1. DAC0   = analog output to PID Module
2. ADC0:6 = analog input from magnetic position sensors and accelerometers.

The application is broken down into Modes of Operation:

1. Manual Control Mode, which allows the user to specify a setpoint for the bottom level. The system will go to that setpoint while monitoring and displaying
all sensor information on the UI
2. Test Builder Mode, which allows the user to build a 'path' for the bottom level to follow. The system then executes the programmed path, collect sensor information, 
and display this on the UI
3. Test Results, which allows the user to open and view previously collected datasets.
4. Mystery Fun Mode, which is just for fun! :) 

NOTE: The application requires the DAQmx driver and assumes the user has a 
simulated DAQ assigned as Dev1 in order to run.
_______________________________________________________________________________
Future Features: 
-Safety features
	-E-stop
	-Check devices/sensors connected
	-Set Max/mins for the Road inputs
-Setting a zero and return to zero at end of test
-Adding a VISA thing to load PID parameters onto device
________________________________________________________________________________
Custom Error Codes:
5000: panel close event - user stopping application
________________________________________________________________________________
Questions:
1. Are patterns like State Machines, Producer/Consumer, or AE, used in OOP?
2. Class parent relationship usage feedback. Too much, too little? 
3. How do folks usually abstract DAQmx hardware? What if for PID loop?
